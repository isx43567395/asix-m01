#! /bin/bash
# @Alejandro López
# Curso 2019 - 2020
# 18-03-2020
# Funcions
# -----------------

#9) showAllGroupMainMembers
function showAllGroupMainMembers(){
  gnames=$(cut -d: -f1 /etc/group | sort)
  for gname in $gnames
  do
    group=$(egrep "^$gname:" /etc/group | cut -d: -f3 )
	users=$(egrep "^[^:]*:[^:]*:[^:]*:$group:" /etc/passwd | cut -d: -f1 | sort | sed -r 's/^(.*)/\t\1/g')

    echo "$gname:"
    echo "$users"
  done
  return 0
}

#11) showAllGroupMainMembers2
function showAllGroupMainMembers2(){
  gnames=$(cut -d: -f1 /etc/group | sort)
  for gname in $gnames
  do
    group=$(egrep "^$gname:" /etc/group | cut -d: -f3 )
    cont_users=$(egrep "^[^:]*:[^:]*:[^:]*:$group:" /etc/passwd | cut -d: -f1 | wc -l)
	users=$(egrep "^[^:]*:[^:]*:[^:]*:$group:" /etc/passwd | cut -d: -f1,3,6,7 | sort -k1g,1 | sed -r 's/^(.*)/\t\1/g')

    echo "$gname($cont_users):"
    echo "$users"
  done
  return 0
}

#18) showGidMembers [file]
function showGidMembers(){
  ERR_NARGS=1
  ERR_NOFILE=2
  OK=0
  if [ $# -gt 1 ]; then
    echo "ERROR: Numero de argumentos incorrecto"
    echo "USAGE: $0 regularFile"
    return $ERR_NARGS
  fi
  fileIn=/dev/stdin
  if [ $# -eq 1 ]; then
    if [ ! -f "$1" ]; then
      echo "ERROR: $file no es un regular file"
      echo "USAGE: $0 regularFile"
      return $ERR_NOFILE
    fi
    fileIn=$1
  fi
  while read -r line
  do
    egrep "^[^:]*:[^:]*:[^:]*:$line:" /etc/passwd | cut -d: -f1,3,6

  done < $fileIn
  return 0	
}

#19) showGidMembers2 [file]
function showGidMembers2(){
  ERR_NARGS=1
  ERR_NOFILE=2
  OK=0
  if [ $# -gt 1 ]; then
    echo "ERROR: Numero de argumentos incorrecto"
    echo "USAGE: $0 regularFile"
    return $ERR_NARGS
  fi
  fileIn=/dev/stdin
  if [ $# -eq 1 ]; then
    if [ ! -f "$1" ]; then
      echo "ERROR: $file no es un regular file"
      echo "USAGE: $0 regularFile"
      return $ERR_NOFILE
    fi
    fileIn=$1
  fi
  while read -r line
  do
    cont_users=$(egrep "^[^:]*:[^:]*:[^:]*:$line:" /etc/passwd | cut -d: -f1 | wc -l)
    if [ $cont_users -ge 3 ]; then
      egrep "^[^:]*:[^:]*:[^:]*:$line:" /etc/passwd | cut -d: -f1,3,6
    fi
  done < $fileIn
  return 0	
}

#20) showPedidos oficina
function showPedidos(){
  ERR_NARGS=1
  ERR_OFICINA=2
  if [ $# -ne 1 ]; then
    echo "ERROR: Numero de argumentos incorrecto"
    echo "USAGE: $0 oficina"
    return $ERR_NARGS
  fi
  
  oficina=$1
  linea=$(cut -f1 oficinas.dat | egrep ^$oficina$) 2> /dev/null
  if [ -z $line ]; then
    echo "Error: Oficina inexistent"
    echo "Usage: $0 oficina"
    return $ERR_OFICINA
  fi
  trabajadores=$(tr '\t' ':' < repventas.dat | egrep "^[^:]*:[^:]*:[^:]*:22:" | cut -d: -f1)
  for trabajador in $trabajadores
  do
	echo "Numero oficina: $oficina"
	echo "Trabajador:$trabajador"
	echo "---Pedidos---"
	tr '\t' ':' < pedidos.dat | egrep "^[^:]*:[^:]*:[^:]*:$trabajador:" | cut -d: -f1
  done
  return 0
}

#21) getHome login
function getHome(){
  status=0
  login=$1
  linea=$(egrep "^$login:" /etc/passwd | cut -d: -f6 2> /dev/null)
  if ! [ -z "$linea" ]; then
     echo $linea
     echo $status
  else
     status=1
     echo $status
  fi
}  	
#22) getHoleList login[...]
function getHomeList(){
  ERR_NARGS=1
  ERR_NOLOGIN=2
  if [ $# -lt 1 ]; then
    echo "ERROR: Numero de argumentos incorrecto"
    echo "USAGE: $0 login[...]"
    return $ERR_NARGS
  fi
  status=0
  logins=$*	
  for login in $logins
  do
    egrep "^$login:" /etc/passwd &> /dev/null
    if [ $? -ne 0 ]; then
       echo "Error: El login $login no existe"
       status=$ERR_NOLOGIN
    else
       getHome $login
    fi
  done
  return $status  	
}
#23) getSize homedir
function getSize(){
  status=0
  login=$1
  linea=$( getHome $login /etc/passwd | head -n1 2> /dev/null)
  if [ -d $linea ]; then
    du -sbh $linea | cut -f1
  else
    status=1
  fi
  return $status
}

#24) getSizeIn
function getSizeIn(){
  ERR_NOLOGIN=1
  status=0
  while read -r line
  do
    linea=$(egrep "^$line:" /etc/passwd | cut -d: -f1 &> /dev/null)
    if [ $? -ne 0 ]; then
       echo "Error: El login $login no existe"
       status=$ERR_NOLOGIN
    else
      getSize $line   
    fi
  
  done
  return status	
}

#25) getAllUsersSize
function getAllUsersSize(){
  lista_users=$(cut -d: -f1 /etc/passwd)
  for user in $lista_users
  do
    echo $user
    getSize $user 2> /dev/null
  
  done
  return 0
}

#26) showShadow login
function showShadow(){
  ERR_NARGS=1
  ERR_NOLOGIN=2
  if [ $# -ne 1 ]; then
    echo "ERROR: Numero de argumentos incorrecto"
    echo "USAGE: $0 login"
    return $ERR_NARGS
  fi
  login=$1
  line=$(egrep "^$login:" /etc/shadow 2> /dev/null)
  if [ $? -ne 0 ]; then
    echo "Error: El login $login no existe"
    return $ERR_NOLOGIN
  fi
  pass=$(echo "$line" | cut -d: -f2)
  ultimoCambio=$(echo "$line" | cut -d: -f3)
  minPassAge=$(echo "$line" | cut -d: -f4)
  maxPassAge=$(echo "$line" | cut -d: -f5)
  passWarning=$(echo "$line" | cut -d: -f6)
  passInactive=$(echo "$line" | cut -d: -f7)
  accExpireDate=$(echo "$line" | cut -d: -f8)
  
  echo "Login: $login"
  echo "Contraseña encriptada: $pass"
  echo "Último cambio: $ultimoCambio"
  echo "Días que faltan hasta poder cambiar la contraseña: $minPassAge"
  echo "Días máximos de contraseña: $maxPassAge"
  echo "Días hasta expiración de contraseña: $passWarning"
  echo "Días desde la expiración de la contraseña: $passInactive"
  echo "Fecha de caducidad de la cuenta: $accExpireDate"
  return 0
}

#27) showShadowList login[...]
function showShadowList(){
  ERR_NARGS=1
  ERR_NOLOGIN=2
  if [ $# -lt 1 ]; then
    echo "ERROR: Numero de argumentos incorrecto"
    echo "USAGE: $0 login[...]"
    return $ERR_NARGS
  fi
  listaLogins=$*
  status=0
  for user in $listaLogins
  do
     line=$(egrep "^$user:" /etc/shadow 2> /dev/null)
     if [ $? -ne 0 ]; then
       echo "Error: El login $user no existe"
       status=$ERR_NOLOGIN
     else
	   pass=$(echo "$line" | cut -d: -f2)
       ultimoCambio=$(echo "$line" | cut -d: -f3)
       minPassAge=$(echo "$line" | cut -d: -f4)
       maxPassAge=$(echo "$line" | cut -d: -f5)
       passWarning=$(echo "$line" | cut -d: -f6)
       passInactive=$(echo "$line" | cut -d: -f7)
       accExpireDate=$(echo "$line" | cut -d: -f8)
       
       echo "Login: $user"
       echo "Contraseña encriptada: $pass"
       echo "Último cambio: $ultimoCambio"
       echo "Días que faltan hasta poder cambiar la contraseña: $minPassAge"
       echo "Días máximos de contraseña: $maxPassAge"
       echo "Días hasta expiración de contraseña: $passWarning"
       echo "Días desde la expiración de la contraseña: $passInactive"
       echo "Fecha de caducidad de la cuenta: $accExpireDate"
    fi 
  done
  return $status
	
}

#28) showShadowIn < login
function showShadowIn(){
  ERR_NOLOGIN=1
  status=0
  while read -r line
  do
     linea=$(egrep "^$line:" /etc/shadow 2> /dev/null)
     if [ $? -ne 0 ]; then
       echo "Error: El login $line no existe"
       status=$ERR_NOLOGIN
     else
	   pass=$(echo "$linea" | cut -d: -f2)
       ultimoCambio=$(echo "$linea" | cut -d: -f3)
       minPassAge=$(echo "$linea" | cut -d: -f4)
       maxPassAge=$(echo "$linea" | cut -d: -f5)
       passWarning=$(echo "$linea" | cut -d: -f6)
       passInactive=$(echo "$linea" | cut -d: -f7)
       accExpireDate=$(echo "$linea" | cut -d: -f8)
       
       echo "Login: $line"
       echo "Contraseña encriptada: $pass"
       echo "Último cambio: $ultimoCambio"
       echo "Días que faltan hasta poder cambiar la contraseña: $minPassAge"
       echo "Días máximos de contraseña: $maxPassAge"
       echo "Días hasta expiración de contraseña: $passWarning"
       echo "Días desde la expiración de la contraseña: $passInactive"
       echo "Fecha de caducidad de la cuenta: $accExpireDate"
    fi 
    
  done
  return $status

}
