#! /bin/bash
# @Alejandro López
# Curso 2019 - 2020
# 23-04-2020
# Funcions disc
# ---------------------------------------

#(1)fsize Donat un login calcular amb du l'ocupació del home de l'usuari. Cal obtenir el home del /etc/passwd.
#showFsize
function fsize(){

  login=$1
  line=$(grep "^$login:" /etc/passwd 2> /dev/null)
  home=$(echo $line | cut -d: -f6)
  du -hs $home 2> /dev/null
  return 0
}

#(2)loginargs Aquesta funció rep logins i per a cada login es mostra l'ocupació de disc del home de l'usuari usant fsize.
#Verificar que es rep almenys un argument. Per a cada argument verificar si és un login vàlid, si no genera una traça d'error.
#loginargs
function loginargs(){

	OK=0
	ERR_NARGS=1
	ERR_NOLOGIN=2

	#Validamos argumento
	if [ $# -lt 1 ];then
	  echo "ERROR: Arguments incorrecte. >=1"
	  echo "USAGE: login login[...]"
	  return $ERR_NARGS
	fi

	for usuari in $*
	do
	  grep "^usuari:" /etc/passwd &> /dev/null
    if [ $? -ne 0 ]; then
     echo "ERROR: el login $usuari no existeix" >> /dev/stderr
     echo "USAGE: login login[...]" >> /dev/stderr
     sortida=$ERR_NOLOGIN
   else
     fsize $usuari
   fi
	done
}

#(3)loginfile Rep com a argument un nom de fitxer que conté un lògin per línia.
#Mostrar l'ocupació de disc de cada
#usuari usant fsize. Verificar que es rep un argument i que és un regular file.

function loginfile(){

  OK=0
	ERR_NARGS=1
  ERR_NOFILE=2
	ERR_NOLOGIN=3

  #Validamos argumento
	if [ $# -ne 1 ];then
	  echo "ERROR: Arguments incorrecte. =1"
	  echo "USAGE: login login[...]"
	  return $ERR_NARGS
	fi

  #Validamos archivo
  if ! [ -f $1 ]; then
    echo "ERROR: $1 no és un regular file." >> /dev/stderr
    echo "USAGE: login file." >> /dev/stderr
    return $ERR_NOFILE
  fi


  file=$1

  while read -r user
  do
    grep "^usuari:" /etc/passwd &> /dev/null
    if [ $? -ne 0 ]; then
     echo "ERROR: el login $usuari no existeix" >> /dev/stderr
     echo "USAGE: login login[...]" >> /dev/stderr
     sortida=$ERR_NOLOGIN
   else
     fsize $user
   fi
  done < $file
  return $OK

}

#(4)loginboth Rep com a argument un file o res (en aquest cas es processa stdin).
# El fitxer o stdin contenen un lògin per línia.
# Mostrar l'ocupació de disc del home de l'usuari. Verificar els arguments rebuts. verificar per cada login rebut que és vàlid.
function loginboth(){

  status=0
  ERR_NARGS=1
  ERR_NOFILE=2
  ERR_NOLOGIN=3

  #Validamos argumento
	if [ $# -gt 1 ];then
	  echo "ERROR: Arguments incorrecte. màxim 1"
	  echo "USAGE: loginboth file"
	  return $ERR_NARGS
	fi

  #Xixa
  file=/dev/stdin
  if [ $# -eq 1 ];then
     if ! [ ! $1 ];then
       echo "ERROR: $1 no és un regular file." >> /dev/stderr
       echo "USAGE: login [file]." >> /dev/stderr
       return $ERR_NOFILE
    fi
  fi

  while read -r user
  do
    grep "^usuari:" /etc/passwd &> /dev/null
    if [ $? -ne 0 ]; then
     echo "ERROR: el login $usuari no existeix" >> /dev/stderr
     echo "USAGE: loginboth [file]" >> /dev/stderr
     status=$ERR_NOLOGIN
   else
     fsize $user
   fi
  done < $file
  return $status
}

#(5)grepgid Donat un GID com a argument, llistar els logins dels usuaris que petanyen a aquest grup com a grup principal.
#Verificar que es rep un argument i que és un GID vàlid.
function grepgid(){

  OK=0
  ERR_NARGS=1
  ERR_GID=2

  #Validamos argumentos
  if [ $# -ne 1 ];then
    echo "ERROR: Arguments incorrecte. ha de ser 1"
    echo "USAGE: grepgid gid"
    return $ERR_NARGS
  fi


  #Validamos gid
  gid=$1
  grep "^[^:]*:[^:]*:$gid:" /etc/group &> /dev/null

  if [ $? -ne 0 ]
  then
    echo "ERROR: El grup $gid no existeix"
    echo "USAGE: grepgid gid."
    return $ERR_GID
  fi

  grep "^[^:]*:[^:]*:[^]*:$gid:" /etc/passwd | cut -d: -f1
  return $OK

}

#(6)gidsize Donat un GID com a argument, mostrar per a cada usuari que pertany a aquest grup l'ocupació de disc del seu home.
#Verificar que es rep un argument i que és un gID vàlid.
function gidsize(){

  OK=0
  ERR_ARGS=1
  ERR_GID=2

  #Validamos args
  if [ $# -ne 1 ];then
    echo "ERROR: Arguments incorrecte. ha de ser 1"
    echo "USAGE: grepsize gid"
    return $ERR_NARGS
  fi



  grep "^[^:]*:[^:]*:$1:" /etc/group &> /dev/null
  if [ $? -ne 0 ]; then
   echo "ERROR: El grup $1 no existeix."
   echo "USAGE: grepgid gid."
   return $ERR_ARGS
  fi

#Xixa
gid=$1
lista_gids=$(grepgid $gid)
for user in $lista_gids
do
  fsize $user

done
return $OK
}

#(7)allgidsize Llistar de tots els GID del sistema (en ordre creixent)
# l'ocupació del home dels usuaris que hi pertanyen.
function allgidsize(){

listaGids=$(cut -d: -f4 /etc/passwd | sort -g | uniq)

for gid in $listaGids
do
  gidsize $gid
done
}


#(8)allgroupsize Llistar totes les línies de /etc/group i per cada línia llistar l'ocupació del home dels usuaris que hi pertanyen.
#Ampliar filtrant només els grups del 0 al 100.
function allgroupsize(){
file=/etc/group
while read -r grup
do
  gid=$(echo $grup | cut -d: -f3)
  if [ $gid -gt 0 -a $gid -le 100 ]
  then
    echo $grup
    users=""
    users=$(echo $grup | cut -d: -f4)

  if ! [ -z $users ]
  then
    for user in $users
    do
      fsize $user
    done
  fi
fi
done < $file
}

#Exercicis fdisk/blkid/fstab

#(9)fstype Donat un fstype llista el device i el mountpoint (per odre de device)
#de les entrades de fstab d'quest fstype.
function fstype(){

fs=$1
file=fstab

while read -r dev
do

  echo $dev | grep "^#" &> /dev/null

  if [ $? -ne 0 ]; then
    llista=$(echo $dev | tr -s '[[:blank:]]' ':' | grep "^[^:]*:[^:]*:$fs:")

    for disp in $llista
    do

    	device=$(echo $disp | cut -d: -f1)
    	mount=$(echo $disp | cut -d: -f2)
		echo "Device: $device Mountpoint: $mount"

    done
  fi
done < $file
}

#(10)allfstype
#LLista per a cada fstype que existeix al fstab (per ordre lexicogràfic)
#les entrades d'quest tipus. Llistar tabuladament el device i el mountpoint.
function allfstype(){
file=fstab

# Fem la llista de File Systems
list_fs=$(cat $file | grep -v "^#" | tr -s '[[:blank:]]' ':' | cut -d: -f3 | sort -u)

for fs in $list_fs
do
  fstype $fs | sed -r 's/(.*)/\t \1/g'
done
}

#(11)allfstypeif LLista per a cada fstype que existeix al fstab (per ordre lexicogràfic)
#les entrades d'quest tipus. Llistar tabuladament el device i el mountpoint.
#Es rep un valor numèric d'argument que indica el numéro mínim d'entrades
#d'aquest fstype que hi ha d'haver per sortir al llistat.
function allfstypeif(){
minim=$1
file=fstab

# Fem la llista de File Systems
list_fs=$(cat $file | grep -v "^#" | tr -s '[[:blank:]]' ':' | cut -d: -f3 | sort -u)

for fs in $list_fs
do

  llista=$(cat $file | grep -v "^#" | tr -s '[[:blank:]]' ':' | grep "^[^:]*:[^:]*:$fs:")

  paraules=$(echo $llista | wc -w)

  if [ $paraules -ge $minim ]; then

    for disp in $llista
    do

      device=$(echo $disp | cut -d: -f1)
      mount=$(echo $disp | cut -d: -f2)
      echo "Device: $device Mountpoint: $mount"

    done
  fi
done
}
