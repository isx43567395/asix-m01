# @Alejandro López
# Curso 2019 - 2020
# 11-04-2020
# Funciones shadow
# -----------------


1) Identificar el contingut del camp password quan encara no se n'ha assignat cap.

user:!!:18363:0:99999:7:::

Si no tiene passwd lo marcará con !!


2) Identificar-ne el contingut quan té un password assignat

user:$6$vsBrbX/l8HCsF4g5$dSYatvpVc1bTogOZdJbDeG/UDpqr/l9aof2skeApvNJasCH/HmB.0Srm4fHvlen2jlgmZM7RYtXj/FSaAqly5.:18363:0:99999:7:::

Al ponerle una passwd, se indica así --> $6$v


3) Identificar-ne el contingut quan el password està bloquejat.

Con el comando -->  passwd -l user

[root@localhost ~]# passwd -l user
Bloqueando la contraseña para el usuario user.
passwd: Éxito

La contraseña bloqueada nos saldrá con dos !! delante de la contraseña cifrada.
!!$6$vsBrbX/l


4) Identificar-ne el contingut quan és un usuari passwordless.

Con el comando -->  passwd -d user

[root@localhost ~]# passwd -d user
Eliminando la contraseña del usuario user.
passwd: Aviso: borrar una contraseña también la desbloquea.
passwd: Éxito

user::18363:0:99999:7:::  --> Ya no nos marca la contraseña porque no tenemos.


5) Bloquejar un compte d'usuari:

Con el comando -->  passwd -l user

user:!!$6$pxP2F2KRnZzdgBPk$/3RNJK4Z5ayLyniBj0mD7gJ4OFs12KIYCjc0MUQaCwdDUQwsVmGP8hI3Wi7xmE4w/ZiSRbIIKWOjVXl2XAfuA1:18363:0:99999:7:::


6) Desbloquejar un compte d'usuari:

Con el comando -->  passwd -u user

[root@localhost ~]# passwd -u user
Desbloqueando la contraseña para el usuario user.
passwd: Éxito


7) Generar un compte d'usuari passwordless:

Con el comando -->  useradd user y passwd -uf user


[root@localhost ~]# passwd -uf user
Desbloqueando la contraseña para el usuario user.
passwd: Éxito


8) Amb l’ordre passwd veure les característiques del password d’un compte d’usuari:

[root@localhost ~]# passwd -S user
user PS 2020-04-11 0 99999 7 -1 (Contraseñaestablecida, cifrado SHA512.)


9) Amb l'ordre chage veure les característiques del password d'un compte d'usuari:

Con el comando -->  chage -l user

[root@localhost ~]# chage -l user
Último cambio de contraseña					:abr 11, 2020
La contraseña caduca					: nunca
Contraseña inactiva					: nunca
La cuenta caduca						: nunca
Número de días mínimo entre cambio de contraseña		: 0
Número de días máximo entre cambio de contraseña		: 99999
Número de días de aviso antes de que caduque la contraseña	: 7


10) Establir a un password una determinada política (max, min, warning, etc).

[root@localhost ~]# passwd -i 5 user
Ajustando los datos de envejecimiento para el usuario user.
passwd: Éxito


11) Modificar a un password valors de la seva política d'expiració:


[root@localhost ~]# passwd -n 1 user
Ajustando los datos de envejecimiento para el usuario user.
passwd: Éxito


En establir requisits a la política de password hem de practicar iniciar sessió amb l'usuari user1 i observar cada una de les següents situacions:

1) No permet canviar el password perquè fa poc que ja el vam canviar:

[user1@localhost alopez]$ passwd
Cambiando la contraseña del usuario user1.
Current password: 
Nueva contraseña: 
CONTRASEÑA INCORRECTA: La contraseña tiene menos de 8 caracteres
Nueva contraseña: 
CONTRASEÑA INCORRECTA: La contraseña tiene menos de 8 caracteres
Nueva contraseña: 
CONTRASEÑA INCORRECTA: La contraseña tiene menos de 8 caracteres
passwd: Se ha agotado el número máximo de reintentos para el servicio


2) Toca canviar el password perquè ja ha passat el període màxim de validesa:

[alopez@localhost ~]$ su user1
Contraseña: 
Debe cambiar la contraseña inmediatamente

3) Mostra un warning informant que cal canviar el password:

[root@localhost ~]# passwd -w 5 user1
Ajustando los datos de envejecimiento para el usuario user1.
passwd: Éxito


4) Obliga a canviar el password en iniciar sessió:

[root@localhost ~]# passwd -e user1
Expirando contraseña para el usuario user1.
passwd: Éxito

[alopez@localhost ~]$ su user1
Contraseña: 
Debe cambiar la contraseña inmediatamente (obligado por el administrador)

5) Ja no permet iniciar sessió:

[root@localhost ~]# passwd -l user1
Bloqueando la contraseña para el usuario user1.
passwd: Éxito

[alopez@localhost ~]$ su user1
Contraseña: 
su: Fallo de autenticación


6) El compte ha expirat:

[alopez@localhost ~]$ su user1
Contraseña: 
La cuenta ha caducado, póngase en contacto con el administrador del sistema
su: La cuenta del usuario ha caducado





