#! /bin/bash
# @Alejandro López
# Curso 2019 - 2020
# 9-04-2020
# Funcions shadow
# -----------------

#26) showShadow login
function showShadow(){
  ERR_NARGS=1
  ERR_NOLOGIN=2
  if [ $# -ne 1 ]; then
    echo "ERROR: Numero de argumentos incorrecto"
    echo "USAGE: $0 login"
    return $ERR_NARGS
  fi
  login=$1
  line=$(egrep "^$login:" /etc/shadow 2> /dev/null)
  if [ $? -ne 0 ]; then
    echo "Error: El login $login no existe"
    return $ERR_NOLOGIN
  fi
  pass=$(echo "$line" | cut -d: -f2)
  ultimoCambio=$(echo "$line" | cut -d: -f3)
  minPassAge=$(echo "$line" | cut -d: -f4)
  maxPassAge=$(echo "$line" | cut -d: -f5)
  passWarning=$(echo "$line" | cut -d: -f6)
  passInactive=$(echo "$line" | cut -d: -f7)
  accExpireDate=$(echo "$line" | cut -d: -f8)
  
  echo "Login: $login"
  echo "Contraseña encriptada: $pass"
  echo "Último cambio: $ultimoCambio"
  echo "Días que faltan hasta poder cambiar la contraseña: $minPassAge"
  echo "Días máximos de contraseña: $maxPassAge"
  echo "Días hasta expiración de contraseña: $passWarning"
  echo "Días desde la expiración de la contraseña: $passInactive"
  echo "Fecha de caducidad de la cuenta: $accExpireDate"
  return 0
}

#27) showShadowList login[...]
function showShadowList(){
  ERR_NARGS=1
  ERR_NOLOGIN=2
  if [ $# -lt 1 ]; then
    echo "ERROR: Numero de argumentos incorrecto"
    echo "USAGE: $0 login[...]"
    return $ERR_NARGS
  fi
  listaLogins=$*
  status=0
  for user in $listaLogins
  do
     line=$(egrep "^$user:" /etc/shadow 2> /dev/null)
     if [ $? -ne 0 ]; then
       echo "Error: El login $user no existe"
       status=$ERR_NOLOGIN
     else
	   pass=$(echo "$line" | cut -d: -f2)
       ultimoCambio=$(echo "$line" | cut -d: -f3)
       minPassAge=$(echo "$line" | cut -d: -f4)
       maxPassAge=$(echo "$line" | cut -d: -f5)
       passWarning=$(echo "$line" | cut -d: -f6)
       passInactive=$(echo "$line" | cut -d: -f7)
       accExpireDate=$(echo "$line" | cut -d: -f8)
       
       echo "Login: $user"
       echo "Contraseña encriptada: $pass"
       echo "Último cambio: $ultimoCambio"
       echo "Días que faltan hasta poder cambiar la contraseña: $minPassAge"
       echo "Días máximos de contraseña: $maxPassAge"
       echo "Días hasta expiración de contraseña: $passWarning"
       echo "Días desde la expiración de la contraseña: $passInactive"
       echo "Fecha de caducidad de la cuenta: $accExpireDate"
    fi 
  done
  return $status
	
}

#28) showShadowIn < login
function showShadowIn(){
  ERR_NOLOGIN=1
  status=0
  while read -r line
  do
     linea=$(egrep "^$line:" /etc/shadow 2> /dev/null)
     if [ $? -ne 0 ]; then
       echo "Error: El login $line no existe"
       status=$ERR_NOLOGIN
     else
	   pass=$(echo "$linea" | cut -d: -f2)
       ultimoCambio=$(echo "$linea" | cut -d: -f3)
       minPassAge=$(echo "$linea" | cut -d: -f4)
       maxPassAge=$(echo "$linea" | cut -d: -f5)
       passWarning=$(echo "$linea" | cut -d: -f6)
       passInactive=$(echo "$linea" | cut -d: -f7)
       accExpireDate=$(echo "$linea" | cut -d: -f8)
       
       echo "Login: $line"
       echo "Contraseña encriptada: $pass"
       echo "Último cambio: $ultimoCambio"
       echo "Días que faltan hasta poder cambiar la contraseña: $minPassAge"
       echo "Días máximos de contraseña: $maxPassAge"
       echo "Días hasta expiración de contraseña: $passWarning"
       echo "Días desde la expiración de la contraseña: $passInactive"
       echo "Fecha de caducidad de la cuenta: $accExpireDate"
    fi 
    
  done
  return $status

}
