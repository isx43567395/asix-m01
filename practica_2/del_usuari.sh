#! /bin/bash
# @Alejandro López
# Curso 2019 - 2020
# 11-04-2020
# del_usuari.sh
# Feu un programa anomenat del_usuari.sh que rep un login i elimina TOT el que
# pertany a l’usuari, però deixant un tar.gz de tots els fitxers que li pertanyen. Va
# generant una traça per stderr de tot allò que va eliminant.
# -----------------
OK=0
ERR_NARGS=1
ERR_NOLOGIN=2
ERR_PROCESOS=3
ERR_IMPRESION=4
ERR_TAR=5
ERR_DELUSER=6
#Comprobación de argumentos
if [ $# -ne 1 ]; then
  echo "ERROR: Numero de argumentos incorrecto"
  echo "USAGE: $0 login"
  exit $ERR_NARGS
fi
login=$1
egrep "^$login:" /etc/passwd &> /dev/null
#Comprobación si existe el login.
if [ $? -ne 0 ]; then
  echo "Error: El login $login no existe"
  exit $ERR_NOLOGIN
fi
#xixa
#Eliminamos procesos
echo "Eliminamos procesos..." >> /dev/stderr
pkill $login &> /dev/null

if [ $? -eq 0 ]; then
  echo "Procesos eliminados correctamente."
else
  echo "ERROR: Los procesos del usuario $login no se han podido eliminar" >> /dev/stderr
  exit $ERR_PROCESOS
fi

#Eliminamos los trabajos de impresión
echo "Eliminamos trabajos de impresión..." >> /dev/stderr
lprm -U $login &> /dev/null
if [ $? -eq 0 ]; then
  echo "Trabajos de impresión eliminados correctamente"
else
  echo "ERROR: Los trabajos de impresión del usuario $login no se han podido eliminar" >> /dev/stderr
  exit $ERR_IMPRESION
fi

#Eliminamos tareas periódicas
echo "Eliminamos tareas periódicas..."
crontab -ru user1 &> /dev/null

#Comprimimos sus archivos (Menos los del home)
echo "Creando backup..." >> /dev/stderr
tar -czvf ficheros.tar.gz $(find / -user $login -type f | grep -v "^/home/$login" &> /dev/null) &> /dev/null
if [ $? -eq 0 ]; then
  echo "Backup realizado con éxito."
else
  echo "ERROR: El backup no se ha podido realizar." >> /dev/stderr
  exit $ERR_TAR
fi

#Eliminamos usuario.
echo "Eliminando usuario..." >> /dev/stderr
userdel -r $login
if [ $? -eq 0 ]; then
  echo "El usuario $login se ha eliminado con éxito."
else
  echo "ERROR: El usuario $login no se ha podido eliminar" >> /dev/stderr
  exit $ERR_DELUSER
fi
exit $OK
