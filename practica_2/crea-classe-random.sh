#! /bin/bash
# @Alejandro López
# Curso 2019 - 2020
# 11-04-2020
# crea-classe-random.sh
# Crea un programa anomenat crea-classe-random.sh que crea alumnes del curs que rep
# com primer argument. Rep un segon o més arguments corresponents a noms d’usuaris a
# donar d’alta en aquesta classe. Cada argument és un login (excepte el primer).
# A cada usuari se li assigna un password random de 8 dígits/caracters.
# Evidentment si es creen els usuaris i se’ls assigna un passwd random si no es desa aquesta
# informació enlloc els usuaris no podran entrar... El programa genera un fitxer passwd.log
# amb l’associació de usuari i passwd generat amb el format login:passwd.
# ----------------------------------------------------------------------------
OK=0
ERR_NARGS=1
ERR_GROUPADD=2
ERR_MKHOME=3
#Comprobación de argumentos
if [ $# -lt 2 ]; then
  echo "ERROR: Numero de argumentos incorrecto"
  echo "USAGE: $0 nombre_clase alum[...]"
  exit $ERR_NARGS
fi
#xixa
clase=$1
shift
#Creamos el grupo
groupadd $clase
if [ $? -eq 0 ]; then
  echo "Grupo $clase creado correctamente."
else
  echo "ERROR: El grupo $clase no se ha podido crear" >> /dev/stderr
  exit $ERR_GROUPADD
fi
#Creamos el directorio de la clase
mkdir -p /home/public/inf/$clase
if [ $? -eq 0 ]; then
  echo "Directorio de la clase creado correctamente ."
else
  echo "ERROR: El directorio no se ha podido crear" >> /dev/stderr
  exit $ERR_MKHOME
fi
dir=/home/public/inf/$clase

#Creación de usuarios.
lista_alumnos=$*
pass=""
for alumno in $lista_alumnos
do
  useradd -N -g $clase -G users -b $dir $alumno
  pass=$(pwgen 8 1)
  echo $pass | passwd $alumno --stdin &> /dev/null
  echo "$alumno:$pass" >> passwd.log
done
exit $OK






