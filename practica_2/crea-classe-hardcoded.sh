#! /bin/bash
# @Alejandro López
# Curso 2019 - 2020
# 11-04-2020
# crea-classe-hardcoded.sh
# Crea un programa anomenat crea-classe-hardcoded.sh que crea 30 alumnes (proveu amb
# 3 per començar...) del curs que rep com a argument.
# Podeu generar els noms dels alumnes automatitzadament amb brace expansion utilitzant
# hisx1-{01-30}.
# Als alumnes els assignem el passwd ‘alum’ per defecte.
# -----------------
OK=0
ERR_NARGS=1
ERR_GROUPADD=2
ERR_MKHOME=3
#Comprobación de argumentos
if [ $# -ne 1 ]; then
  echo "ERROR: Numero de argumentos incorrecto"
  echo "USAGE: $0 nombre_clase"
  exit $ERR_NARGS
fi

#xixa
clase=$1
pass="alum"
#Creamos el grupo
groupadd $clase
if [ $? -eq 0 ]; then
  echo "Grupo $clase creado correctamente."
else
  echo "ERROR: El grupo $clase no se ha podido crear" >> /dev/stderr
  exit $ERR_GROUPADD
fi

#Creamos el directorio de la clase
mkdir -p /home/public/inf/$clase
if [ $? -eq 0 ]; then
  echo "Directorio de la clase creado correctamente ."
else
  echo "ERROR: El directorio no se ha podido crear" >> /dev/stderr
  exit $ERR_MKHOME
fi
dir=/home/public/inf/$clase

#Creación de usuarios.
nombres_alumnos=$( echo $clase-{01..02})
for alumno in $nombres_alumnos
do
  useradd -N -g $clase -G users -b $dir $alumno
  echo $pass | passwd $alumno --stdin &> /dev/null
done
exit 0
