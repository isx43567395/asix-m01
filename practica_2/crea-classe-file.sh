#! /bin/bash
# @Alejandro López
# Curso 2019 - 2020
# 11-04-2020
# crea-classe-file.sh
# Crea un programa anomenat crea-classe-file.sh que crea alumnes del curs que rep com a
# primer argument. Rep un segon argument corresponent a un fitxer amb noms:password
# d’alumnes a donar d’alta.
# Es tracta de crear la classe amb el nom indicat per el primer argument i afegir-hi els
# alumnes que conté el fitxer rebut com a segon argument. Aquest fitxer conté per a cada línia
# el nom de l’usuari i el seu password amb el format nom:passwd.
# -----------------

OK=0
ERR_NARGS=1
ERR_FILE=2
ERR_GROUPADD=3
ERR_MKHOME=4
#Comprobación de argumentos
if [ $# -ne 1 ]; then
  echo "ERROR: Numero de argumentos incorrecto"
  echo "USAGE: $0 nombre_clase"
  exit $ERR_NARGS
fi

#Validamos que sea un archivo.
if ! [ -f $2 ]; then
  echo "ERROR: $2 No es un fichero"
  echo "USAGE: $0 Nombre_clase fichero"
fi

#xixa
clase=$1
archivo=$2

#Creamos el grupo
groupadd $clase
if [ $? -eq 0 ]; then
  echo "Grupo $clase creado correctamente."
else
  echo "ERROR: El grupo $clase no se ha podido crear" >> /dev/stderr
  exit $ERR_GROUPADD
fi

#Creamos el directorio de la clase
mkdir -p /home/public/inf/$clase
if [ $? -eq 0 ]; then
  echo "Directorio de la clase creado correctamente ."
else
  echo "ERROR: El directorio no se ha podido crear" >> /dev/stderr
  exit $ERR_MKHOME
fi
dir=/home/public/inf/$clase

while read -r alumno
do
  nombre=$(echo $alumno | cut -d: -f1)
  pass=$(echo $alumno | cut -d: -f2)
  useradd -N -g $clase -G users -b $dir $nombre
  echo $pass | passwd $nombre --stdin &> /dev/null

done < $archivo
exit 0
